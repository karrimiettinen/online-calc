#!/bin/bash

BASE_HREF="/public"

. .env # load dotenv values
echo "setting BASE_HREF to: \"$BASE_HREF\""

rm -Rf dist
mkdir dist
cp -r public/* dist
echo "const BASE_HREF = \"$BASE_HREF\";" > dist/config.js