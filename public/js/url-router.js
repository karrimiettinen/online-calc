/**
 * @typedef {Object} Route
 * @property {string} path
 * @property {string} template
 * @property {string} component
 * @property {string} title
 */

/**
 * @type {Array<Route>}
 */
const urlRoutes = [
    {
        path: "404",
        template: `${BASE_HREF}/templates/404.html`,
        title: "Not found"
    },
    {
        path: `${BASE_HREF}/`,
        template: `${BASE_HREF}/templates/main.html`,
        title: "Online tools"
    },
    {
        path: `${BASE_HREF}/math`,
        component: `${BASE_HREF}/views/math/math.js`,
        cname: "comp-math",
        title: "Math"
    },
    {
        path: `${BASE_HREF}/screen`,
        template: `${BASE_HREF}/views/screen/screen.js`,
        cname: "comp-screen",
        title: "Screen-size"
    }
];

const urlLocationHandler = async () => {
    const location = window.location.pathname;
    if (location.length == 0) location = `${BASE_HREF}/`;
    const route = urlRoutes.find((r) => r.path == location) || urlRoutes[0];
    let html = "";
    if (route.component) {
        html = `<${route.cname}></${route.cname}>`;
    } else if (route.template) {
        html = await fetch(route.template)
            .then((response) => response.text());
    }
    document.getElementById("view-port").innerHTML = html;
    document.getElementById("title-content").innerText = route.title;
    document.title = route.title;
};

/**
 * url routing function
 * @param {MouseEvent} event 
 */
const urlRoute = (event) => {
    event = event || window.event;
    event.preventDefault();
    window.history.pushState({}, "", event.target.href);
    urlLocationHandler();
};

document.addEventListener("click", (e) => {
    const { target } = e;
    if (!target.matches("nav a")) {
        return;
    }
    e.preventDefault();
    urlRoute(e);
});
