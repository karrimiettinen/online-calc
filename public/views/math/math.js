class Operation {
    /** @type {HTMLInputElement} */
    term1
    /** @type {HTMLInputElement} */
    term2
    /** @type {HTMLInputElement} */
    result
    constructor(input_t1_elem, input_t2_elem, result_elem) {
        this.term1 = input_t1_elem;
        this.term2 = input_t2_elem;
        this.result = result_elem;
    }
}

class MathComponent extends HTMLElement {
    /** @type {Operation} */
    add_elems = undefined;
    /** @type {Operation} */
    sub_elems = undefined;
    /** @type {Operation} */
    mul_elems = undefined;
    /** @type {Operation} */
    div_elems = undefined;
    /** @type {Operation} */
    constructor(){
        super();
        this.cname = "math";
        this.shadow = this.attachShadow({ mode: "open" });
    } 
   
    async connectedCallback() {
        const styles = await fetch(`${BASE_HREF}/views/${this.cname}/${this.cname}.css`)
            .then((response) => response.text());
        const html = await fetch(`${BASE_HREF}/views/${this.cname}/${this.cname}.html`)
            .then((response) => response.text());
        this.view = `<style>
            ${styles}
            </style>
            ${html}
        `;
        this.render();
        const add_btn = this.shadow.querySelector("#add");
        add_btn.addEventListener('click', this.addition.bind(this));
        this.add_elems = new Operation(
            this.shadow.querySelector("#addend1"),
            this.shadow.querySelector("#addend2"),
            this.shadow.querySelector("#sum")
        );
        const sub_btn = this.shadow.querySelector("#sub");
        sub_btn.addEventListener('click', this.subtract.bind(this));
        this.sub_elems = new Operation(
            this.shadow.querySelector("#minuend"),
            this.shadow.querySelector("#subtrahend"),
            this.shadow.querySelector("#difference"),
        );
        const mul_btn = this.shadow.querySelector("#mul");
        mul_btn.addEventListener('click', this.multiply.bind(this));
        this.mul_elems = new Operation(
            this.shadow.querySelector("#multiplier"),
            this.shadow.querySelector("#multiplicand"),
            this.shadow.querySelector("#product")
        );
        const div_btn = this.shadow.querySelector("#div");
        div_btn.addEventListener('click', this.divide.bind(this));
        this.div_elems = new Operation(
            this.shadow.querySelector("#dividend"),
            this.shadow.querySelector("#divisor"),
            this.shadow.querySelector("#quotient")
        )
    }

    render() {
        this.shadow.innerHTML = this.view;
    }

    /**
     * @param {Operation} elems 
     * @returns {Array<number>}
     */
    getValues(elems) {
        const a1 = parseFloat(elems.term1.value);
        const a2 = parseFloat(elems.term2.value);
        return [a1, a2];
    }

    addition() {
        const values = this.getValues(this.add_elems);
        const result = values[0] + values[1];
        this.add_elems.result.value = result.toString();
    }
    
    subtract() {
        const values = this.getValues(this.sub_elems);
        const result = values[0] - values[1];
        this.sub_elems.result.value = result.toString();
    }

    multiply() {
        const values = this.getValues(this.mul_elems);
        const result = values[0] * values[1];
        this.mul_elems.result.value = result.toString();
    }
    
    divide() {
        const values = this.getValues(this.div_elems);
        const result = values[0] / values[1];
        this.div_elems.result.value = result.toString();
    }
}

window.customElements.define('comp-math', MathComponent);
